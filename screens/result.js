import { View, Text,StyleSheet,TouchableOpacity, FlatList, Image } from 'react-native';
import React,{useState, useEffect} from 'react';
import { data } from '../components/quetionsData';
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function RESULT( {navigation} ) {

  const [numberOfCorrectAns,setNumberOfCorrectAns] = useState(0);

    const response = navigation.getParam("Response");
    const correctAnswers = navigation.getParam("CorrectAnswers");
    const userName = navigation.getParam("UserName");
    const screenChangeFrom  = navigation.getParam("ScreenChangeFrom");

    useEffect(() => {
      countCorrectAns();
      setData();
    })
    
    const countCorrectAns = () => {
      let count = 0;
      for(let index=0; index < response.length; index++){
        if(response[index] === correctAnswers[index]){
          count += 1;
        }
      }
      setNumberOfCorrectAns(count);
    }

    const setData = () => {
      AsyncStorage.getItem("userdetails").then(async (value) => {
        let temp = true;
        if(value !== null){
        value = JSON.parse(value);
        for(let index = 0; index < value.length; index++){
          if(userName === value[index]["userName"]){
            temp = false;
            break;
          }
        }
      }
      if(temp === true){
        if(value === null){
          await AsyncStorage.setItem("userdetails" , JSON.stringify([{"userName" : userName,"score" : numberOfCorrectAns,"response" : response,"correctAnswers" : correctAnswers}]));
        }
        else{ 
        value.push({"userName" : userName,"score" : numberOfCorrectAns,"response" : response,"correctAnswers" : correctAnswers});
        await AsyncStorage.setItem("userdetails" , JSON.stringify(value));
        }
      }
      })
    }

    const goBackToHome = () => {
      navigation.navigate("Home");
    }

  return (
    <View style={{flex:1,backgroundColor:"white"}}>
      <View style={{flex:0.15,backgroundColor:"white",alignSelf:"stretch",alignItems:"center",justifyContent:"space-around",margin:10}}>
      <Text style={styles.headerText}>{screenChangeFrom === "quetions" ? "Thanks for taking the test": "User Name: " + userName}</Text>
      <Text style={styles.headerText}>your score {numberOfCorrectAns}</Text>
      <Text style={styles.headerText}>your Response</Text>
      </View>
      <View style={{flex:1,backgroundColor:"white"}}>
      <FlatList
        data={data["questionsData"]}
        renderItem={({item,index}) => { return(
            <View style={{margin:15,marginBottom:0,flex:1}}>
            <View style={{flexDirection:"row",justifyContent:"space-between"}}> 
            <Text style={{fontSize:20,fontWeight:"500",marginBottom:10,color:"black"}}>{index + 1}. {item["question"]}</Text>
            <Text style={{right:30}}>{response[index] !== correctAnswers[index] ? <Image style={{width:25,height:25,borderRadius:40}} source={require("../components/crossSymbol.jpg")}/>:<Image style={{width:25,height:25,borderRadius:40}} source={require("../components/rightSymbol.jpg")}/>}</Text>
            </View>
            <View>
            <Text style={styles.optionsText}>A. {item["options"][0]["option"]}</Text>
            <Text style={styles.optionsText}>B. {item["options"][1]["option"]}</Text>
            <Text style={styles.optionsText}>C. {item["options"][2]["option"]}</Text>
            <Text style={styles.optionsText}>D. {item["options"][3]["option"]}</Text>
            <Text style={{fontSize:17,fontWeight:"500",marginBottom:10,alignSelf:"center",...{color:response[index] !== correctAnswers[index] ? "red":"green"}}}>Response: {response[index]} | Answer: {correctAnswers[index]}</Text>
            </View>
            </View>
         )
        }
        }
        />
      </View>
      <View style={{flex:0.1,flexDirection:"row",alignSelf:"stretch",justifyContent:"space-between",alignItems:"center",marginLeft:10,marginRight:10}}>
        <TouchableOpacity onPress={goBackToHome}>
        <View style={{backgroundColor:"cyan",padding:8,borderRadius:5}}>
        <Text style={{fontSize:15,color:"black",fontWeight:"500"}}>TAKE TEST AGAIN</Text>
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Previous_score")}>
        <View style={{backgroundColor:"white",padding:8,borderRadius:5}}>
        <Text style={{fontSize:17,color:"black",margin:2,fontWeight:"500"}}>Previous Scores</Text>
        </View>
        </TouchableOpacity>
        </View>
    </View>
  )
}


const styles = StyleSheet.create(
    {
        headerText : {fontSize:17,fontWeight:"500",color:"black"},
        optionsText : {fontSize:17,marginBottom:10,marginLeft:22,color:"black",fontWeight:"500"}
    }
)