import { View, Text, Image,TouchableOpacity,Modal, TextInput, Alert  } from 'react-native';
import React, {useState,useEffect} from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function HOME( {navigation} ) {
    const [modalControl, setModal] = useState(false);
    const [userName, setUserName] = useState("");
    const [alertMssg, setAlertMssg] = useState("")

    useEffect(() => {
      validateUserName(userName)
    },[userName])

    const validateUserName = (name) => {
        AsyncStorage.getItem("userdetails").then( value => {
        let temp = true;
        if(value !== null){
        value = JSON.parse(value);
        for(let index = 0; index < value.length; index++){
          if(name === value[index]["userName"]){
            temp = false;
            break;
          }
        }
      }
        if(temp === true){
          setAlertMssg("")
        }
        else{
          setAlertMssg("Name Alredy Existed");
        }
      })
    }

    const ChangeScreenToQuetions = () => {
      if(alertMssg === ""){
        setModal(false);
        navigation.navigate("Quetions",{UserName:userName});
      }
    }

  return (
    <View style={{flex:1,backgroundColor:"skyblue",alignItems:"center"}}>
        <Modal visible={modalControl} transparent={true}>
            <View style={{backgroundColor:"#000000aa",flex:1,alignItems:"center",justifyContent:"center"}}>
                <View style={{backgroundColor:"white",borderRadius:10,alignSelf:"stretch",marginLeft:50,marginRight:50,borderColor:"black"}}>
                <View style={{margin:20,marginLeft:40,marginRight:40,alignSelf:"stretch",alignItems:"center"}}>
                <Text style={{fontSize:20,color:"black"}}>Please Enter Your Name</Text>
                <TextInput style={{borderBottomColor:"black",borderBottomWidth:1,alignSelf:"stretch",fontSize:20,textAlign:"center"}} onChangeText={(value) => setUserName(value.trim())}/>
                <View>
                  <Text style={{color:"red"}}>{alertMssg}</Text>
                </View>
                <View style={{flexDirection:"row",alignSelf:"stretch",justifyContent:"space-around",marginTop:10}}>
                <TouchableOpacity onPress={() => setModal(false)}>
                <Text style={{fontSize:20,color:"black"}}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity disabled={userName.length > 0 && userName.length < 20 ? false : true} onPress={ChangeScreenToQuetions}>
                <Text style={{fontSize:20,color:"black"}}>Confirm</Text>
                </TouchableOpacity>
                </View>
                </View>
                </View>
            </View>
        </Modal>
      <View style={{flex:1,alignSelf:"stretch",alignItems:"center",justifyContent:"flex-start"}}>
      <Text style={{fontSize:27,fontWeight:"bold",color:"black",margin:35}}>Quiz App</Text>
      </View>
      <View style={{flex:1,alignSelf:"stretch",alignItems:"center",justifyContent:"center"}}>
      <Image style={{width:140,height:140,borderRadius:40}} source={require("../screens/appLogo.png")}/>
      </View>
      <View style={{flex:1,alignSelf:"stretch",justifyContent:"flex-end"}}>
      <TouchableOpacity onPress={() => setModal(true)}>
      <View style={{padding:15,alignItems:"center",backgroundColor:"white",justifyContent:"center",alignSelf:"stretch",marginHorizontal:50,borderRadius:5}}>
        <Text style={{fontSize:20,fontWeight:"400",color:"black"}}>Take Test</Text>
      </View>
      </TouchableOpacity>
      </View>
      <View style={{flex:0.4,alignSelf:"stretch",alignItems:"center"}}>
        <TouchableOpacity onPress={() => navigation.navigate("Previous_score")}>
        <Text style={{padding:20,fontSize:20,fontWeight:"400",color:"black"}}>Previous Scores</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

