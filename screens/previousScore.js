import { View, Text,FlatList, TouchableOpacity } from 'react-native'
import React, { useEffect,useState } from 'react'
import AsyncStorage from "@react-native-async-storage/async-storage"


export default function PREVIOUS( {navigation} ) {
  const [allUserDetails, setallUserDetails] = useState([]);

  useEffect(() => {
    getData();
  },[])

  const getData = () =>{
    AsyncStorage.getItem("userdetails").then(value =>{
      if(value != null){
      value = JSON.parse(value);
      setallUserDetails(value);
      }
    })
  }

  const changeScreenToResult = (previousData) => {
      const response = previousData["response"]
      const correctAnswers = previousData["correctAnswers"]
      const userName = previousData["userName"]
      navigation.navigate("Result",{Response:response,CorrectAnswers:correctAnswers,UserName:userName,ScreenChangeFrom:"previousScore"})
  }

  return (
    <View style={{flex:1,backgroundColor:"white"}}>
      <View style={{flex:0.09,flexDirection:"row",backgroundColor:"white",borderBottomWidth:1,alignSelf:"stretch",alignItems:"center",justifyContent:"space-around"}}>
        <Text style={{fontSize:20,color:"black",fontWeight:"bold"}}>Name</Text>
        <Text style={{fontSize:20,color:"black",fontWeight:"bold"}}>Score</Text>
      </View>
      <FlatList
      style={{flex:1,backgroundColor:"white",marginBottom:20}}
      data = {allUserDetails}
      renderItem = {({item,index}) => { return(
        <TouchableOpacity onPress={() => changeScreenToResult(item)} style={{flex:0.09,flexDirection:"row",backgroundColor:"skyblue",alignSelf:"stretch",alignItems:"center",justifyContent:"space-between",margin:20,marginBottom:0,borderRadius:20}}>
          <Text style={{fontSize:20,color:"black",fontWeight:"400",marginLeft:40}}>{index + 1}.  {item["userName"]}</Text>
          <Text style={{fontSize:20,color:"black",fontWeight:"400",marginRight:70}}>{item["score"]}</Text>
        </TouchableOpacity>
  )}}
      />
      </View>
  )
}