import { View, Text, TouchableOpacity,StyleSheet,ScrollView, FlatList } from 'react-native';
import React, {useState,useEffect} from 'react';
import RADIOBUTTON from '../components/radioButton';
import { data } from '../components/quetionsData';


export default function QUETIONS( {navigation} ) {
      
      const [currentQuetionNumber,changeQuetionNumber] = useState(0);
      const [radioCheck,changeRadio] = useState(false);
      const [previousAns,setAns] = useState([]);
      const [response,setResponse] = useState([]);
      const [correctAnswers,setCorrectAnswers] = useState([]);
      const [selectedQuetion,setSelectedQuetion] = useState(1);
      const [quetionNumber,setquetionNumber] = useState([]);
      let question = data["questionsData"][currentQuetionNumber]["question"];

      useEffect(() => {
        let numberOfQuetions = []
        let quetionNumber = [];
        for(let index = 0; index < data["questionsData"].length;index++){
          numberOfQuetions.push(false);
          quetionNumber.push(index + 1)
        }
        setAns(numberOfQuetions);
        setquetionNumber(quetionNumber)
      },[])

      const userName = navigation.getParam("UserName");

      const changeScreenToResult = () => {
        if(radioCheck !== false){
        previousAns[currentQuetionNumber] = radioCheck;
        setAns(previousAns);
        validateAnswers();
        navigation.replace("Result",{Response:response,CorrectAnswers:correctAnswers,UserName:userName,ScreenChangeFrom:"quetions"});
      }
    }

      const validateAnswers = () => {
        for(let index = 0;index < previousAns.length; index++){
          response.push(data["questionsData"][index]["options"][previousAns[index]]["option"]);
          for(let index1 = 0; index1 < data["questionsData"][index]["options"].length; index1++){
            if(data["questionsData"][index]["options"][index1]["option_id"] === data["questionsData"][index]["correct_option"]){
              correctAnswers.push(data["questionsData"][index]["options"][index1]["option"]);
              break;
            }
          }
        }
        setResponse(response);
        setCorrectAnswers(correctAnswers);
      }

      const changeQuetion = (number) => {
        setSelectedQuetion(number);
        changeQuetionNumber(number - 1);
        changeRadio(previousAns[number - 1]);
      }
      
      const previous = () => {
          setSelectedQuetion(currentQuetionNumber);
          changeQuetionNumber(currentQuetionNumber - 1);
          changeRadio(previousAns[currentQuetionNumber - 1]);
      }
      const next = () => {
            setSelectedQuetion(currentQuetionNumber + 2);
            changeQuetionNumber(currentQuetionNumber + 1);
            changeRadio(previousAns[currentQuetionNumber + 1]);
      }
      
      const storeData = (option) => {
        changeRadio(option);
        previousAns[currentQuetionNumber] = option;
        setAns(previousAns);
      }

      const onclick = (value) => {
        changeRadio(value);
      }
  return (
    <View style={{flex:1,borderWidth:2,borderRadius:2}}>
    <View style={{flex:0.07,backgroundColor:"skyblue"}}>
      <FlatList 
      contentContainerStyle = {{flex: 1, alignItems:"center", justifyContent: 'center',marginTop:10}}
      horizontal
      data = {quetionNumber}
      renderItem={({item}) => { return(
        <TouchableOpacity disabled={previousAns[item - 2] === false ? true : false} onPress={() => changeQuetion(item)}>
        <View style={{...{backgroundColor: item === selectedQuetion ? "gray" : previousAns[item - 1] === false ? "white" : "green"},...{opacity:item === selectedQuetion || previousAns[item - 2] !== false ? 1 : 0.2},borderWidth:2,width:30,height:30,borderRadius:40,alignItems:"center",marginLeft:15}}>
        <Text style={{fontSize:18,fontWeight:"bold",color:"black"}}>{item}</Text>
        </View>
        </TouchableOpacity>
      )}}/>
       
    </View>
    <View style={{flex:1,backgroundColor:"skyblue"}}>
        <View style={{flex:1,backgroundColor:"white",margin:20,borderRadius:20,borderColor:"black",borderWidth:2}}>
          <ScrollView horizontal={true} style={{marginLeft:20,marginRight:20}}>
            <View style={{justifyContent:"center"}}>
            <Text style={{fontSize:30,fontWeight:"bold",color:"black"}}>{currentQuetionNumber + 1}. {question}</Text>
            </View>
          </ScrollView>
        </View>

        <View style={{flex:3}}>
        <FlatList
        contentContainerStyle = {{height:"100%",justifyContent:"space-around"}}
        data = {data["questionsData"][currentQuetionNumber]["options"]}
        renderItem= {({item,index}) => {
          return(
            <TouchableOpacity style={styles.optionsView} onPress={() => storeData(index)}>
            <RADIOBUTTON id={index} condition={radioCheck === index ? true:false} onPressOption={onclick} />
            <Text style={styles.optionsText}>{item["option"]}</Text>
            </TouchableOpacity>
          )
        }}
        />
        </View>

        <View style={{flex:0.7,flexDirection:"row",alignSelf:"stretch",justifyContent:"space-around",alignItems:"center"}}>
        <TouchableOpacity disabled={currentQuetionNumber === 0 ? true : false} onPress={previous}>
        <View style={{backgroundColor:"white",padding:10,borderRadius:20,borderColor:"black",borderWidth:2,...{opacity: currentQuetionNumber === 0 ? 0.3 : 1}}}>
        <Text style={{fontSize:20,fontWeight:"bold",color:"black"}}>Previous</Text>
        </View>
        </TouchableOpacity>
        <TouchableOpacity disabled={radioCheck === false ? true : false} onPress={currentQuetionNumber === (previousAns.length - 1) ? changeScreenToResult: next}>
        <View style={{backgroundColor:"white",padding:10,paddingHorizontal:35,borderRadius:20,borderColor:"black",borderWidth:2,...{opacity: previousAns[currentQuetionNumber] === false ? 0.3 : 1}}}>
        <Text style={{fontSize:20,fontWeight:"bold",color:"black"}}>{currentQuetionNumber === (previousAns.length - 1) ? "Submit":  "Next"}</Text>
        </View>
        </TouchableOpacity>
        </View>
    </View>
    </View>
  )
}

const styles = StyleSheet.create(
    {
        optionsView:{flexDirection:"row",backgroundColor:"white",borderRadius:20,borderColor:"black",borderWidth:2,alignItems:"center",marginLeft:20,marginRight:20},
        optionsText:{fontSize:20,fontWeight:"bold",left:20,color:"black",marginTop:10,marginBottom:10}
    }
)