import { View, TouchableOpacity } from 'react-native'
import React from 'react'

export default function RADIOBUTTON(props) {
  const {onPressOption, condition, id} = props;
  return (
    <TouchableOpacity onPress={() => onPressOption(id)}>
    <View style={{backgroundColor:"white",borderWidth:2,width:30,height:30,borderRadius:40,left:10}}>
      <View style={{...{backgroundColor:condition === false ?"white":"black"},width:20,height:20,alignSelf:"center",marginTop:3,borderRadius:40}}>
      </View>
    </View>
    </TouchableOpacity>
  )
}