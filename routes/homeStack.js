import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import HOME from "../screens/home";
import QUETIONS from "../screens/quetions";
import RESULT from "../screens/result";
import PREVIOUS from "../screens/previousScore";

const screens = {
    Home : {
        screen: HOME,
    },
    Quetions : {
        screen: QUETIONS
    },
    Result : {
        screen: RESULT
    },
    Previous_score : {
        screen: PREVIOUS,
        navigationOptions:{headerShown: true,title:"Previous Score",headerTitleAlign:"center",headerStyle:{backgroundColor:"skyblue"}}
    }
}

const HomeStack = createStackNavigator(screens,{defaultNavigationOptions:{
    headerShown: false
}});
export default createAppContainer(HomeStack)